﻿using AutoMapper;
using ChallengeN5_ApiRest.DTO;
using ChallengeN5_ApiRest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeN5_ApiRest.Utilities
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper<T1, T2>()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<T1, T2>().ReverseMap();
                cfg.CreateMap<Guid, string>().ConvertUsing(o => o.ToString());
                cfg.CreateMap<Permissions, PermissionsDto>();                
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }
    }
}
