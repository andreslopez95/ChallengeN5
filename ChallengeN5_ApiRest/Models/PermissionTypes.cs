﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeN5_ApiRest.Models
{
    public class PermissionTypes
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
