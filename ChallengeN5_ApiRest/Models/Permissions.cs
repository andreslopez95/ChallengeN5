﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeN5_ApiRest.Models
{
    public class Permissions
    {
        public int Id { get; set; }
        public string EmployeeForename { get; set; }
        public string EmployeeSurname { get; set; }
        public int PermissionTypeId { get; set; }
        [ForeignKey("PermissionTypeId")]
        public virtual PermissionTypes PermissionTypes { get; set; }
        public DateTime PermissionDate { get; set; }
    }
}
