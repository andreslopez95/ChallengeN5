﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChallengeN5_ApiRest.DTO;
using ChallengeN5_ApiRest.Models;
using ChallengeN5_ApiRest.Persistence;
using ChallengeN5_ApiRest.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Nest;

namespace ChallengeN5_ApiRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionsController : ControllerBase
    {        
        private readonly IMSRepository<Permissions> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ElasticClient _client;
        private readonly ILogger<PermissionsController> _logger;

        public PermissionsController(IMSRepository<Permissions> repository, IUnitOfWork unitOfWork, ElasticClient client, ILogger<PermissionsController> logger)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _client = client;
            _logger = logger;
        }
        
        [HttpGet]
        public async Task<IQueryable<Permissions>> GetPermissions()
        {
            _logger.LogInformation("Called GetPermissions");

            try { 
                return await _repository.GetAllAsync(include: source => source.Include(x => x.PermissionTypes));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in GetPermissions");
                return (IQueryable<Permissions>)NotFound();
            }
        }
        
        /// <summary>
        /// PermissionTypes Options: 1 super admin, 2 admin, 3 support. 
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Permissions>> RequestPermission(PermissionsDto Dto)
        {
            _logger.LogInformation("Called RequestPermission");

            try
            {            
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                var oMapper = AutoMapperConfig.GetMapper<Permissions, PermissionsDto>().Map<Permissions>(Dto);
                await _repository.CreateAsync(oMapper);
                _unitOfWork.Commit();

                return CreatedAtAction(nameof(GetPermissions), new { id = oMapper.Id }, oMapper);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in RequestPermission");
                return NotFound();
            }
        }

        [HttpPut]
        public async Task<IActionResult> ModifyPermission(PermissionsDto Dto)
        {
            _logger.LogInformation("Called ModifyPermission");

            try { 
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.Id.Equals(Dto.Id));

                var oMapper = AutoMapperConfig.GetMapper<Permissions, PermissionsDto>().Map<Permissions>(Dto);
                await _repository.UpdateAsync(oMapper);
                _unitOfWork.Commit();

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in ModifyPermission");
                return NotFound();
            }
        }
    }
}
