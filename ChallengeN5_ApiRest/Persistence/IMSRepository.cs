﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ChallengeN5_ApiRest.Persistence
{
    public interface IMSRepository<T> where T : class
    {
        Task<IQueryable<T>> GetAllAsync(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,            
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        /*Task<IQueryable<T>> GetAsync(
            Expression<Func<T, bool>> whereCondition = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "");*/

        Task<T> FirstOrDefaultAsync(
            Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        Task<bool> CreateAsync(T entity);

        Task<bool> UpdateAsync(T entity);
    }
}
