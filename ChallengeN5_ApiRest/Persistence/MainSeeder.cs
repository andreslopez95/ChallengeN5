﻿using ChallengeN5_ApiRest.Context;
using ChallengeN5_ApiRest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeN5_ApiRest.Persistence
{
    public class MainSeeder
    {
        private readonly ApplicationDbContext _context;

        public MainSeeder(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task SeedAsync()
        {
            await _context.Database.EnsureCreatedAsync();

            if (_context.PermissionTypes.Count() <= 0)
            {
                await PermissionTypesAsync();
            }
        }

        #region Métodos
        private async Task PermissionTypesAsync()
        {
            var permissionTypes = new List<PermissionTypes>()
            {
                new PermissionTypes { Description = "super admin"},
                new PermissionTypes { Description = "admin"},
                new PermissionTypes { Description = "support"},
            };

            await _context.AddRangeAsync(permissionTypes);
            await _context.SaveChangesAsync();
        }
        #endregion
    }
}
