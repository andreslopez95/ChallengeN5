﻿using ChallengeN5_ApiRest.Models;
using Microsoft.EntityFrameworkCore;

namespace ChallengeN5_ApiRest.Context
{
    public class ApplicationDbContext : DbContext
    {
        #region Colecciones BD
        public DbSet<Permissions> Permissions { get; set; }
        public DbSet<PermissionTypes> PermissionTypes { get; set; }
        #endregion

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
