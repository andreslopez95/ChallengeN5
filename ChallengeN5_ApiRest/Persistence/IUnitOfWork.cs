﻿using ChallengeN5_ApiRest.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeN5_ApiRest.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationDbContext Context { get;  }
        void Commit();
    }    
}
